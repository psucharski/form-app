import { combineReducers } from 'redux';
import formReducer from './formReducer';
import submissionReducer from './submissionReducer';

export default combineReducers({
  formReducer,
  submissionReducer
});
