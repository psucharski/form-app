import _ from 'lodash';

export default (state = {}, action) => {
  switch (action.type) {
    case 'SET_FIELD':
    case 'SET_POSITION':
    case 'SET_IS_VALID':
      const copiedState = JSON.parse(JSON.stringify(state)); //make sure we have a deep copy

      return _.merge(copiedState, action.payload);
    default:
      return state;
  }
};
