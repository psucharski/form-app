export default {
    fields: [
        { type: 'Input', label: 'Name', name: 'name' },
        { type: 'Input', label: 'Nickname', name: 'nickname' },
        { type: 'Input', label: 'Email', name: 'email', inputType: 'email', validator: 'email' },
        {
            type: 'Select',
            options: [
                { value: 'IT' },
                { value: 'Product' },
                { value: 'Content' }
            ],
            label: 'Field',
            name: 'field'
        },
        {
            type: 'Select',
            options: [
                { value: 'Front-end developer', category: 'IT' },
                { value: 'Back-end developer', category: 'IT' },
                { value: 'DevOps', category: 'IT' },
                { value: 'Webmaster', category: 'IT' },
                { value: 'Product owner', category: 'Product' },
                { value: 'UX Designer', category: 'Product' },
                { value: 'UI Designer', category: 'Product' },
                { value: 'Junior Copywriter', category: 'Content' },
                { value: 'Senior Copywriter', category: 'Content' },
            ],
            label: 'Position',
            name: 'position',
            filterByParent: true,
            filterField: 'field'
        }
    ]
}