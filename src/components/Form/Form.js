import React, { memo } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import { setFieldValidity, submitForm } from '../../actions/formActions';
import * as validators from '../../services/validators';

import Input from '../Input';
import Select from '../Select';
import config from './config';
import './Form.scss';

export function Form({ storeFields, submitFormValues, validateField }) {
  const validateFields = () => {
    config.fields.forEach((field) => {
      const fieldValue = storeFields[field.name].value;

      if (fieldValue && (field.validator ? validators[field.validator].test(fieldValue) : true)) {
        validateField(field.name, true);
      } else {
        validateField(field.name, false);
      }
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    validateFields();

    if (Object.keys(storeFields).every((fieldName) => storeFields[fieldName].isValid)) {
      submitFormValues(storeFields);
    }
  };

  const generateFields = () =>
    config.fields.map(
      (field, i) => (field.type === 'Select' ? <Select {...field} key={i} /> : <Input {...field} key={i} />)
    );

  return (
    <form onSubmit={handleSubmit} className={'Form'}>
      {generateFields()}
      <button type="submit" className={'Form__button--submit'}>
        Submit
      </button>
    </form>
  );
}

const mapStateToProps = (state) => {
  return {
    storeFields: state.formReducer
  };
};

const mapDispatchToProps = (dispatch) => ({
  validateField: (name, value) => dispatch(setFieldValidity(name, value)),
  submitFormValues: (values) => dispatch(submitForm(values))
});

function arePropsEqual(nextProps, prevProps) {
  const propsToCompare = [ 'storeFields' ];
  //compare only 'non-function props
  return _.isEqual(_.pick(nextProps, propsToCompare), _.pick(prevProps, propsToCompare));
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(Form, arePropsEqual));
