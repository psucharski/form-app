import React, { memo } from 'react';
import _ from 'lodash';
import './Input.scss';
import { setFormValue, setFieldValidity } from '../../actions/formActions';
import { connect } from 'react-redux';
import * as validators from '../../services/validators';

export function Input({
  name,
  inputType = 'text',
  label,
  setFieldValue,
  value,
  validator,
  setFieldValidation,
  isValid
}) {
  const handleChange = (event) => {
    setFieldValue(event.target.value);
  };

  const handleValidation = () => {
    if (value && (validator ? validators[validator].test(value) : true)) {
      setFieldValidation(true);
    } else {
      setFieldValidation(false);
    }
  };

  return (
    <div className={'Field__text'}>
      <label htmlFor={name}>{label}</label>
      <input
        className={isValid ? 'Field__text--valid' : 'Field__text--invalid'}
        name={name}
        type={inputType}
        value={value}
        onChange={handleChange}
        onBlur={handleValidation}
        required={true}
        autoComplete={name}
      />
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    value: _.get(state, `formReducer[${ownProps.name}].value`, ''),
    isValid: _.get(state, `formReducer[${ownProps.name}].isValid`, false)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  setFieldValue: (value) => dispatch(setFormValue(ownProps.name, value)),
  setFieldValidation: (isValid) => dispatch(setFieldValidity(ownProps.name, isValid))
});

export default connect(mapStateToProps, mapDispatchToProps)(memo(Input));
