import React, { useEffect, memo } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import Option from './Option';
import { setFormValue, setFieldValidity } from '../../actions/formActions';
import './Select.scss';

export function Select({
  name,
  label,
  category,
  value,
  setFieldValue,
  filterByParent = false,
  options,
  setFieldValidation,
  isValid
}) {
  const handleChange = (event) => {
    setFieldValue(event.target.value);
  };

  useEffect(
    () => {
      if (filterByParent && value) {
        setFieldValue('');
      }
    },
    [ category ]
  );

  const handleValidation = () => {
    if (value) {
      setFieldValidation(true);
    } else {
      setFieldValidation(false);
    }
  };

  const optionMarkup = ({ value }, i) => <Option key={i + 1} value={value} />;

  const generateOptions = () =>
    filterByParent
      ? options.filter((val) => val.category.toLowerCase() === category).map(optionMarkup)
      : options.map(optionMarkup);

  return (
    <div className={'Field__select'}>
      <label htmlFor={name}>{label}</label>
      <select
        name={name}
        value={value}
        onChange={handleChange}
        className={isValid ? 'Field__select--valid' : 'Field__select--invalid'}
        onBlur={handleValidation}
      >
        <Option value="" key={0} />
        {generateOptions()}
      </select>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  category: _.get(state, `formReducer[${ownProps.filterField}].value`, ''),
  value: _.get(state, `formReducer[${ownProps.name}].value`),
  isValid: _.get(state, `formReducer[${ownProps.name}].isValid`)
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  setFieldValue: (value) => dispatch(setFormValue(ownProps.name, value)),
  setFieldValidation: (isValid) => dispatch(setFieldValidity(ownProps.name, isValid))
});

export default connect(mapStateToProps, mapDispatchToProps)(memo(Select));
