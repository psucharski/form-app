import React from 'react';

const Option = ({ key, value }) => (
  <option key={key} value={value.toLowerCase()}>
    {value}
  </option>
);

export default Option;
