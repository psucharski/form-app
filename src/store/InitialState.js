export default {
  formReducer: {
    name: { value: '', isValid: false },
    nickname: { value: '', isValid: false },
    email: { value: '', isValid: false },
    field: { value: '', isValid: false },
    position: { value: '', isValid: false }
  }
};
