import React from 'react';
import { connect } from 'react-redux';
import './App.scss';
import Form from './components/Form';

function App({ submitedValues }) {
  return (
    <div className="App">
      <header className="App-header">Form Builder</header>
      <Form />
      {Object.keys(submitedValues).length ? JSON.stringify(submitedValues) : ''}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    submitedValues: state.submissionReducer || {}
  };
};

export default connect(mapStateToProps)(App);
