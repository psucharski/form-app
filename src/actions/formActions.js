export const setFormValue = (fieldName, value) => ({
  type: 'SET_FIELD',
  payload: { [fieldName]: { value } }
});

export const setFieldValidity = (fieldName, isValid) => ({
  type: 'SET_IS_VALID',
  payload: { [fieldName]: { isValid } }
});

export const submitForm = (formData) => {
  return {
    type: 'SUBMIT_FORM',
    payload: formData
  };
};
